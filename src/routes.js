export default routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('app', {
      url: '/',
      component: 'app',
      redirectTo: 'app.welcome'
    })
    .state('app.welcome', {
      url: 'welcome',
      component: 'welcome'
    })
    .state('app.platforms', {
      url: 'platforms',
      component: 'platformList'
    })
    .state('app.plans', {
      url: 'plans?platformSku',
      component: 'planList'
    })
    .state('app.register', {
      url: 'plans/register?planSku',
      component: 'planRegisterForm'
    });
}
