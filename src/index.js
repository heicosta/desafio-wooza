import angular from 'angular';

import {PlatformService} from './app/services/platforms/platforms';
import {PlanService} from './app/services/plans/plans';
import {App} from './app/containers/App';
import {Header} from './app/components/header/Header';
import {MainSection} from './app/components/main/MainSection';
import {Welcome} from './app/components/welcome/Welcome';
import {PlatformList} from './app/components/platforms/PlatformList';
import {PlanList} from './app/components/plans/PlanList';
import {PlanRegisterForm} from './app/components/plans/planRegisterForm/PlanRegisterForm';
import {Footer} from './app/components/footer/Footer';
import 'angular-ui-router';
import 'ngstorage';
import routesConfig from './routes';

import './styles/index.scss';

angular
  .module('app', ['ui.router', 'ngStorage'])
  .config(routesConfig)
  .service('platformService', PlatformService)
  .service('planService', PlanService)
  .component('app', App)
  .component('headerComponent', Header)
  .component('footerComponent', Footer)
  .component('mainSection', MainSection)
  .component('welcome', Welcome)
  .component('platformList', PlatformList)
  .component('planList', PlanList)
  .component('planRegisterForm', PlanRegisterForm);
