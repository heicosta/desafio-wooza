export class PlanService {

  /** @ngInject */
  constructor($http, $log, $sessionStorage, $state) {
    this.$http = $http;
    this.$log = $log;
    this.$storage = $sessionStorage;
    this.$state = $state;
  }

  async listByPlatformSku(platformSku) {
    const response = await this.$http.get(`http://private-59658d-celulardireto2017.apiary-mock.com/planos/${platformSku}`).catch(err => {
      return {error: true, errorMsg: `Error on load Plans: ${angular.toJson(err)}`};
    });
    this.$log.info(`[PlanService] Response listAll: ${angular.toJson(response)}`);
    return response.data.planos;
  }

  storagePlan(plan) {
    this.$storage.plan = plan;
  }

  getSelectedPlan() {
    return this.$storage.plan;
  }

  getSelectedPlatform() {
    return this.$storage.platform;
  }

  goState(state, paramObj) {
    return this.$state.go(state, paramObj);
  }
}
