export class PlatformService {

  /** @ngInject */
  constructor($http, $log, $sessionStorage) {
    this.$http = $http;
    this.$log = $log;
    this.$storage = $sessionStorage;
  }

  async listAll() {
    const response = await this.$http.get('http://private-59658d-celulardireto2017.apiary-mock.com/plataformas').catch(err => {
      return {error: true, errorMsg: `Error on load response: ${angular.toJson(err)}`};
    });
    this.$log.info(`[PlatformService] Response listAll: ${angular.toJson(response)}`);
    return response.data.plataformas;
  }

  storagePlatform(platform) {
    this.$storage.platform = platform;
  }

  getSelectedPlatform() {
    return this.$storage.platform;
  }
}
