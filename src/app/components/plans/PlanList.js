class PlanListController {
  /** @ngInject */
  constructor($log, $scope, $stateParams, planService) {
    this.$log = $log;
    this.$scope = $scope;
    this.$stateParams = $stateParams;
    this.planService = planService;
    this.plans = null;
    this.selectedPlatform = null;
    this.onInit();
  }

  onInit() {
    this.selectedPlatform = this.planService.getSelectedPlatform();
    this.loadPlans();
  }

  async loadPlans() {
    const platformSku = this.$stateParams.platformSku;
    const response = await this.planService.listByPlatformSku(platformSku);
    if (response.error) {
      this.$log.error(response.errorMsg);
    }
    this.plans = response;
    this.$scope.$apply();
  }

  selectPlan(plan) {
    this.planService.storagePlan(plan);
    this.planService.goState('app.register', {platformSku: this.selectedPlatform.sku, planSku: plan.sku});
  }
}

export const PlanList = {
  template: require('./PlanList.html'),
  controller: PlanListController
};
