class PlanRegisterFormController {
  /** @ngInject */
  constructor($log, $scope, $stateParams, planService) {
    this.$log = $log;
    this.$scope = $scope;
    this.$stateParams = $stateParams;
    this.planService = planService;
    this.contractPlanRegister = null;
    this.selectedPlatform = null;
    this.selectedPlan = null;
    this.onInit();
  }

  onInit() {
    this.selectedPlatform = this.planService.getSelectedPlatform();
    this.selectedPlan = this.planService.getSelectedPlan();
  }

  async submitForm() {
    this.$log.log('$ Plano contratado com SUCESSO!');
    this.$log.log(`# Plataforma: ${this.selectedPlatform.sku} - ${this.selectedPlatform.nome}`);
    this.$log.log(`# Plano: ${this.selectedPlan.sku} - ${this.selectedPlan.franquia} - ${this.selectedPlan.descricao}`);
    this.$log.log('# Contrato');
    this.$log.log(`## Contrato.email : ${this.contractPlanRegister.email}`);
    this.$log.log(`## Contrato.cpf : ${this.contractPlanRegister.cpf}`);
    this.$log.log(`## Contrato.nome : ${this.contractPlanRegister.nome}`);
    this.$log.log(`## Contrato.endereco : ${this.contractPlanRegister.endereco}`);
  }
}

export const PlanRegisterForm = {
  template: require('./PlanRegisterForm.html'),
  controller: PlanRegisterFormController
};
