class PlatformListController {
  /** @ngInject */
  constructor($log, $scope, $state, platformService) {
    this.$log = $log;
    this.$scope = $scope;
    this.$state = $state;
    this.platformService = platformService;
    this.platforms = null;
    this.onInit();
  }

  onInit() {
    this.loadPlatforms();
  }

  async loadPlatforms() {
    const platforms = await this.platformService.listAll();
    if (platforms.error) {
      this.$log.error(platforms.errorMsg);
    }
    this.platforms = platforms;
    this.$scope.$apply();
  }

  platformSelected(platform) {
    this.platformService.storagePlatform(platform);
    this.$state.go('app.plans', {platformSku: platform.sku});
  }
}

export const PlatformList = {
  template: require('./PlatformList.html'),
  controller: PlatformListController
};
