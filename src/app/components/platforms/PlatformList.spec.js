import angular from 'angular';
import angular from 'angular';
import 'angular-mocks';
import {PlatformList} from './PlatformList';

describe('PlatformList component', () => {
  class MockPlatformService {
    listAll() {}    
  }

  let component;

  beforeEach(() => {
    angular
      .module('platformList', ['app/components/platforms/PlatformList.js'])
      .service('platformService', MockPlatformService)
      .component('platformList', PlatformList);
    angular.mock.module('platformList');
  });

  beforeEach(angular.mock.inject($componentController => {
    component = $componentController('platformList', {}, {});
  }));

  it('shoud call listAll', () => {
    spyOn(component.platformService, 'listAll').and.callThrough();
    component.handleListAll();
    expect(component.platformService.listAll).toHaveBeenCalled();
  });
});
