class HeaderController {
  /** @ngInject */
  constructor($log) {
    this.$log = $log;
  }
}

export const Header = {
  template: require('./Header.html'),
  controller: HeaderController
};
