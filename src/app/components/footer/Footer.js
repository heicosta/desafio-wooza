class FooterController {
  /** @ngInject */
  constructor($log) {
    this.$log = $log;
    this.onInit();
  }

  onInit() {
    this.$log.info('Footer component loaded...');
  }
}

export const Footer = {
  template: require('./Footer.html'),
  controller: FooterController
};
