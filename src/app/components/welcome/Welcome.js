class WelcomeController {
  /** @ngInject */
  constructor($log) {
    this.$log = $log;
    this.onInit();
  }

  onInit() {
    this.$log.info('Welcome Component loaded...');
  }
}

export const Welcome = {
  template: require('./Welcome.html'),
  controller: WelcomeController
};
