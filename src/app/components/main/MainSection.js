class MainSectionController {
  /** @ngInject */
  constructor($log) {
    this.$log = $log;
    this.onInit();
  }

  onInit() {
    this.$log.info('MainSection Component loaded...');
  }
}

export const MainSection = {
  template: require('./MainSection.html'),
  controller: MainSectionController
};
