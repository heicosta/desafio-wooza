class AppController {
  /** @ngInject */
  constructor($log) {
    this.$log = $log;
    this.onInit();
  }

  onInit() {
    this.$log.info('Wooza Challange App Componentloaded...');
  }
}

export const App = {
  template: require('./App.html'),
  controller: AppController
};
