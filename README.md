![Marca da Wooza](src/assets/images/logo-wooza.png "Wooza")

# Olá! Eu sou Heitor Costa e esta é a minha aplicação web construída para o teste da vaga para Desenvolvedor Front-End Sênior.

A aplicação foi escrita em AngularJS 1.6, JavaScript ES6 (com Babel traduzindo para Vanilla), utilizando Webpack + Gulp no build, SASS para a escrita de CSS e Bootstrap para decorar o tema.
Para saber mais sobre a arquitetura utilizada.

A aplicação é basicamente um CRUD, que tem seu fluxo iniciado na listagem de Plataformas. Escolhida uma plataforma, é apresentada uma lista de Planos. Finalmente, ao selecionar um Plano, é exibido um formulário básico de cadastro com Nome, E-mail, Nascimento, CPF e Telefone.

Ao submeter o formulário, o resultado do envio é exibido no console, conforme orientação do teste.

# Execução da aplicação em ambiente local

## Considerando já instalados o NodeJS (https://nodejs.org/en) e o GulpJ (https://www.gulpjs.com), executar as seguintes linhas de comando no terminal, na raíz do projeto:

### npm i
### npm run serve
### Acesse a url http://localhost:3000
### Clique em "Começar" !